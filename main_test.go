package main

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

var ApiKey = "12345678" //test api key
var UserId = 123        //test user id
var TestTime = int32(time.Now().Unix())
var srv Server

func TestMain(test *testing.M) {
	srv = Server{}
	srv.Init("rest_api", "rest_api", "rest_api", false)

	code := test.Run()

	os.Exit(code)
}

func TestInsertion(test *testing.T) {

	payload := []byte(fmt.Sprintf(`{"api_key":"%s","user_id":%d,"timestamp":%d,"action":"testing"}`, ApiKey, UserId, TestTime))

	req, _ := http.NewRequest("POST", "/api", bytes.NewBuffer(payload))
	resp := executeRequest(req)

	checkResponseCode(test, http.StatusCreated, resp.Code)
}

func TestSelectionWithApiKey(test *testing.T) {

	req, _ := http.NewRequest("GET", "/api/"+ApiKey, nil)
	resp := executeRequest(req)

	checkResponseCode(test, http.StatusOK, resp.Code)
}

func TestSelectionWithApiAndId(test *testing.T) {
	req, _ := http.NewRequest("GET", fmt.Sprintf("/api/%s/%d", ApiKey, UserId), nil)
	resp := executeRequest(req)

	checkResponseCode(test, http.StatusOK, resp.Code)
}

func TestDeletion(test *testing.T) {
	TestInsertion(test)

	payload := []byte(fmt.Sprintf(`{"api_key":"%s","user_id":%d,"timestamp":%d,"action":"testing"}`, ApiKey, UserId, TestTime))

	req, _ := http.NewRequest("DELETE", "/api", bytes.NewBuffer(payload))
	resp := executeRequest(req)

	checkResponseCode(test, http.StatusOK, resp.Code)
}

func TestUpdate(test *testing.T) {
	TestInsertion(test)

	var TimeT = int32(time.Now().Unix())
	payload := []byte(fmt.Sprintf(`{"api_key":"%s","user_id":%d,"timestamp":%d,"action":"testing updated v2"}`, ApiKey, UserId, TimeT))

	req, _ := http.NewRequest("PUT", "/api", bytes.NewBuffer(payload))
	resp := executeRequest(req)

	checkResponseCode(test, http.StatusOK, resp.Code)
}

func TestUnauthorizedAccess(test *testing.T) {
	payload := []byte(fmt.Sprintf(`{"api_key":"%s","user_id":%d,"timestamp":%d,"action":"testing updated v3"}`, "ErronousAPIKEY", UserId, TestTime))

	req, _ := http.NewRequest("PUT", "/api", bytes.NewBuffer(payload))
	resp := executeRequest(req)

	checkResponseCode(test, http.StatusForbidden, resp.Code)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	srv.Router.ServeHTTP(rr, req)
	return rr
}

func checkResponseCode(test *testing.T, expected, actual int) {
	if expected != actual {
		test.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
