package main

/**
Server handles goes here (boxing of database handles)
*/
import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"github.com/wcharczuk/go-chart"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
)

type Keys struct {
	Keys []string `json:"ids"`
}

type Server struct {
	Router       *mux.Router
	DBConnection *sql.DB
	port         int32
	AuthKeys     []string
}

func (srv *Server) Init(user, passwd, dbName string, wipeTables bool) {
	conStr := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable", user, passwd, dbName) // read these from json config
	var err error
	srv.DBConnection, err = sql.Open("postgres", conStr)
	if err != nil {
		log.Fatal(err)
	}

	//FIXED
	var tableQuery = "CREATE TABLE IF NOT EXISTS Records (api_key CHAR(8) NOT NULL, user_id NUMERIC(10,0), timestamp NUMERIC(10,0) NOT NULL, action VARCHAR(99),id SERIAL, CONSTRAINT Records_pkey PRIMARY KEY (id))"
	if _, err := srv.DBConnection.Exec(tableQuery); err != nil {
		log.Fatal(err) //check if table exists
	}

	var respTimeTableQuery = "CREATE TABLE IF NOT EXISTS ResponseTimes (id INTEGER REFERENCES Records(id) ON DELETE CASCADE,response_time DECIMAL(7,4) NOT NULL)"

	if _, err := srv.DBConnection.Exec(respTimeTableQuery); err != nil {
		log.Fatal(err)
	}

	srv.WipeTables()

	jsonFile, err := os.Open("api_keys.json") //update keys to store them in database as well
	// dont read keys from config.json

	if err != nil {
		log.Fatal(err)
	}

	var keys Keys
	jsonBytes, _ := ioutil.ReadAll(jsonFile)

	jsonErr := json.Unmarshal(jsonBytes, &(keys))

	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	srv.AuthKeys = keys.Keys

	jsonFile.Close()

	//fmt.Println(int32(time.Now().Unix())) // for now this stays here

	srv.Router = mux.NewRouter()
	var p int32 = 8080
	srv.port = p
	srv.initURLs()

}

func (srv *Server) WipeTables() {
	_, err := srv.DBConnection.Exec("DELETE FROM ResponseTimes")
	if err != nil {
		log.Fatal(err)
	}

	_, err = srv.DBConnection.Exec("DELETE FROM Records")
	if err != nil {
		log.Fatal(err)
	}

	_, err = srv.DBConnection.Exec("ALTER SEQUENCE records_id_seq RESTART WITH 1")
	if err != nil {
		log.Fatal(err)
	}

}

func (srv *Server) StartServer() {
	fmt.Printf("Trying to serve on %d\n", srv.port)
	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)

	go func() {
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", srv.port), srv.Router))
	}()

	<-stop

	fmt.Println("Shutting down...")

	srv.DBConnection.Close()
}

func (srv *Server) initURLs() {
	srv.Router.NotFoundHandler = http.HandlerFunc(NotFound)
	srv.Router.HandleFunc("/api/visualize", srv.visualizeResponseTimes).Methods("GET")
	srv.Router.HandleFunc("/api/{api_key:.*}/{id:[0-9]+}", srv.getEventsWithApiAndId).Methods("GET")
	srv.Router.HandleFunc("/api/{api_key:.*}", srv.getEventsWithApiKey).Methods("GET")
	srv.Router.HandleFunc("/api", srv.update).Methods("PUT")
	srv.Router.HandleFunc("/api", srv.insert).Methods("POST")
	srv.Router.HandleFunc("/api", srv.delete).Methods("DELETE")
}

func (srv *Server) insert(respWriter http.ResponseWriter, request *http.Request) {

	var event Event

	decoder := json.NewDecoder(request.Body)

	if err := decoder.Decode(&event); err != nil {
		errorResponse(respWriter, http.StatusBadRequest, "Invalid payload")
		return
	}

	defer request.Body.Close()

	if !srv.isKeyAuthorized(event.ApiKey) {
		errorResponse(respWriter, http.StatusForbidden, "Requested data is forbidden with API_KEY :"+event.ApiKey)
		return
	}
	//TODO: finish this function

	if err := event.handleInsert(srv.DBConnection); err != nil {
		errorResponse(respWriter, http.StatusInternalServerError, err.Error())
		return
	}

	wrapJSON(respWriter, http.StatusCreated, event)
}

func (srv *Server) delete(respWriter http.ResponseWriter, request *http.Request) {

	var event Event

	decoder := json.NewDecoder(request.Body)

	if err := decoder.Decode(&event); err != nil {
		errorResponse(respWriter, http.StatusBadRequest, "Invalid payload")
		return
	}

	defer request.Body.Close()

	if !srv.isKeyAuthorized(event.ApiKey) {
		errorResponse(respWriter, http.StatusForbidden, "Requested data is forbidden with API_KEY :"+event.ApiKey)
		return
	}
	//TODO: finish this function

	if err := event.handleDelete(srv.DBConnection); err != nil {
		if err.Error() == "Resource Not Found" {
			errorResponse(respWriter, http.StatusNotFound, "Requested resource is not found")
		} else {
			errorResponse(respWriter, http.StatusInternalServerError, err.Error())

		}
		return
	}

	wrapJSON(respWriter, http.StatusOK, map[string]string{"result": "delete successful"})

}

func (srv *Server) update(respWriter http.ResponseWriter, request *http.Request) {

	var event Event

	decoder := json.NewDecoder(request.Body)

	if err := decoder.Decode(&event); err != nil {
		errorResponse(respWriter, http.StatusBadRequest, "Invalid payload")
		return
	}

	defer request.Body.Close()

	if !srv.isKeyAuthorized(event.ApiKey) {
		errorResponse(respWriter, http.StatusForbidden, "Requested data is forbidden with API_KEY :"+event.ApiKey)
		return
	}
	//TODO: finish this function

	if err := event.handleUpdate(srv.DBConnection); err != nil {
		if err.Error() == "Resource Not Found" {
			errorResponse(respWriter, http.StatusNotFound, "Requested resource is not found")
		} else {
			errorResponse(respWriter, http.StatusInternalServerError, err.Error())

		}
		return
	}

	wrapJSON(respWriter, http.StatusOK, event)

}

func (srv *Server) getEventsWithApiAndId(respWriter http.ResponseWriter, request *http.Request) {
	variables := mux.Vars(request)
	api_key := variables["api_key"] //Update this so that anyone can query with timestamp or action (better to filter
	// out some of the events with WHERE)

	if !srv.isKeyAuthorized(api_key) {
		errorResponse(respWriter, http.StatusForbidden, "Requested data is forbidden with API_KEY :"+api_key)
		return
	}

	id, err := strconv.Atoi(variables["id"])

	if err != nil {
		errorResponse(respWriter, http.StatusBadRequest, "Bad ID number")
		return
	}

	event := Event{UserId: id, ApiKey: api_key}

	if events, err := event.getEventsForApiAndId(srv.DBConnection); err == nil && len(events) != 0 {
		wrapJSON(respWriter, http.StatusOK, events)
	} else if len(events) == 0 {
		errorResponse(respWriter, http.StatusNotFound, fmt.Sprintf("No content found for API_KEY %s and id %d", api_key, id))
	} else {
		errorResponse(respWriter, http.StatusInternalServerError, err.Error())
	}
}

/**
this function might be redundant delete later ?
*/
func (srv *Server) getEventsWithApiKey(respWriter http.ResponseWriter, request *http.Request) {
	variables := mux.Vars(request)
	api_key := variables["api_key"]
	if !srv.isKeyAuthorized(api_key) {
		errorResponse(respWriter, http.StatusForbidden, "Requested data is forbidden with API_KEY :"+api_key)
		return
	}

	event := Event{ApiKey: api_key}

	if events, err := event.getAllEventsForApiKey(srv.DBConnection); err == nil {
		wrapJSON(respWriter, http.StatusOK, events)
	} else if len(events) == 0 {
		errorResponse(respWriter, http.StatusNotFound, "No content found for API_KEY "+api_key)
	} else {
		errorResponse(respWriter, http.StatusInternalServerError, err.Error())
	}

}

func (srv *Server) visualizeResponseTimes(respWriter http.ResponseWriter, request *http.Request) {
	//TODO fix this so that values are collected from database
	var zeroToOne = "SELECT COUNT(*) FROM ResponseTimes WHERE response_time BETWEEN 0 AND 1"
	var OneToFive = "SELECT COUNT(*) FROM ResponseTimes WHERE response_time BETWEEN 1 AND 5"
	var FiveToTen = "SELECT COUNT(*) FROM ResponseTimes WHERE response_time BETWEEN 5 AND 10"
	var TenToTwenty = "SELECT COUNT(*) FROM ResponseTimes WHERE response_time BETWEEN 10 AND 20"
	var TwentyToFifty = "SELECT COUNT(*) FROM ResponseTimes WHERE response_time BETWEEN 20 AND 50"
	var FiftyToHundred = "SELECT COUNT(*) FROM ResponseTimes WHERE response_time BETWEEN 50 AND 100"
	rows := srv.DBConnection.QueryRow(zeroToOne)
	var count1 float64
	rows.Scan(&count1)
	rows2 := srv.DBConnection.QueryRow(OneToFive)
	var count2 float64
	rows2.Scan(&count2)
	rows3 := srv.DBConnection.QueryRow(FiveToTen)
	var count3 float64
	rows3.Scan(&count3)
	rows4 := srv.DBConnection.QueryRow(TenToTwenty)
	var count4 float64
	rows4.Scan(&count4)
	rows5 := srv.DBConnection.QueryRow(TwentyToFifty)
	var count5 float64
	rows5.Scan(&count5)
	rows6 := srv.DBConnection.QueryRow(FiftyToHundred)
	var count6 float64
	rows6.Scan(&count6)
	if count1 == 0.0 && count2 == 0.0 && count3 == 0.0 && count4 == 0.0 && count5 == 0.0 && count6 == 0.0 {
		count1 = 100.0
		fmt.Println("Database was empty so all events are logically below 1 since there is no events")
	}
	sbc := chart.BarChart{
		Title:      "Distribution of Response Times of Events",
		TitleStyle: chart.StyleShow(),
		Background: chart.Style{
			Padding: chart.Box{
				Top: 40,
			},
		},
		Height:   512,
		BarWidth: 60,
		XAxis: chart.Style{
			Show: true,
		},
		YAxis: chart.YAxis{
			Style: chart.Style{
				Show: true,
			},
		},
		Bars: []chart.Value{
			{Value: count1, Label: "<1ms"},
			{Value: count2, Label: "<5ms"},
			{Value: count3, Label: "<10ms"},
			{Value: count4, Label: "<20ms"},
			{Value: count5, Label: "<50ms"},
			{Value: count6, Label: "<100ms"},
		},
	}

	respWriter.Header().Set("Content-Type", "image/png")
	err := sbc.Render(chart.PNG, respWriter)
	if err != nil {
		log.Fatal(err)
	}

}

func NotFound(w http.ResponseWriter, r *http.Request) { //notfound statement
	var myVar []string
	myVar = append(myVar, "Error", "Requested element cannot be found (aka 404 Not Found)")
	resp, _ := json.Marshal(myVar)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	w.Write(resp)
}

func errorResponse(respWriter http.ResponseWriter, code int, message string) {
	wrapJSON(respWriter, code, map[string]string{"error": message})
}

func wrapJSON(respWriter http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	respWriter.Header().Set("Content-Type", "application/json")
	respWriter.WriteHeader(code)
	respWriter.Write(response)
}

func (srv *Server) isKeyAuthorized(api_key string) bool {
	var cond = false
	for _, str := range srv.AuthKeys {
		if str == api_key {
			cond = true
			break
		}
	}
	return cond
}
