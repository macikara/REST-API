package main

/**
Database handles goes here
*/
import (
	"database/sql"
	"fmt"
	"log"
	rand2 "math/rand"
	"time"
)

type errStr struct {
	e string
}

func (e *errStr) Error() string {
	return e.e
}

type Event struct { //struct that will hold information about an event
	ApiKey    string `json:"api_key"`
	UserId    int    `json:"user_id"`
	Timestamp int32  `json:"timestamp"`
	Action    string `json:"action"`
}

func (event *Event) getEventsForApiAndId(DBConnection *sql.DB) ([]Event, error) {
	rows, err := DBConnection.Query(fmt.Sprintf("SELECT user_id, timestamp, action FROM Records WHERE api_key='%s' AND user_id=%d", event.ApiKey, event.UserId))
	if err != nil {
		return nil, err
	}

	tempEvents := []Event{}

	for rows.Next() {
		var event Event
		if err := rows.Scan(&event.UserId, &event.Timestamp, &event.Action); err != nil {
			rows.Close()
			return nil, err
		}
		tempEvents = append(tempEvents, event)
	}
	rows.Close()
	return tempEvents, nil
}

func (event *Event) handleInsert(DBConnection *sql.DB) error {

	row := DBConnection.QueryRow(fmt.Sprintf("INSERT INTO Records (api_key,user_id,timestamp,action) VALUES ('%s',%d,%d,'%s') RETURNING id", event.ApiKey, event.UserId, event.Timestamp, event.Action))

	var id int
	err := row.Scan(&id)
	if err != nil {
		log.Fatal(err)
	}
	randomSleepInterval(DBConnection, id)

	return nil
}

func (event *Event) handleDelete(DBConnection *sql.DB) error {
	res, err := event.getEventsForApiAndId(DBConnection)

	if err != nil {
		return err
	}
	if len(res) == 0 {
		return &errStr{e: "Resource Not Found"}
	}
	_, err = DBConnection.Exec(fmt.Sprintf("DELETE FROM Records WHERE api_key='%s' AND user_id=%d AND timestamp=%d AND action='%s'", event.ApiKey, event.UserId, event.Timestamp, event.Action))

	if err != nil {
		return err
	}
	return nil
}

func (event *Event) handleUpdate(DBConnection *sql.DB) error {
	returned, err := DBConnection.Exec(fmt.Sprintf("UPDATE Records SET action='%s', timestamp=%d WHERE api_key='%s' AND user_id=%d", event.Action, event.Timestamp, event.ApiKey, event.UserId))
	affected, _ := returned.RowsAffected()

	if affected == 0 {
		return &errStr{e: "Resource Not Found"}
	}
	if err != nil {
		return err
	}

	return nil
}

func (event *Event) getAllEventsForApiKey(DBConnection *sql.DB) ([]Event, error) {
	rows, err := DBConnection.Query(fmt.Sprintf("SELECT user_id, timestamp, action FROM Records WHERE api_key='%s'", event.ApiKey))
	if err != nil {
		return nil, err
	}

	tempEvents := []Event{}

	for rows.Next() {
		var event Event
		if err := rows.Scan(&event.UserId, &event.Timestamp, &event.Action); err != nil {
			rows.Close()
			return nil, err
		}
		tempEvents = append(tempEvents, event)
	}
	rows.Close()
	if len(tempEvents) == 0 {
		return nil, &errStr{e: "Resource Not Found"}
	}
	return tempEvents, nil
}

func randomSleepInterval(DBConnection *sql.DB, id int) {
	rand := rand2.Intn(100)
	start := time.Now()
	time.Sleep(time.Duration(rand) * time.Millisecond)
	end := time.Now()
	elapsed := end.Sub(start)
	millis := float32(elapsed.Nanoseconds()) / 1E6 // this is milliseconds

	_, err := DBConnection.Exec(fmt.Sprintf("INSERT INTO ResponseTimes VALUES (%d,%3.4f)", id, millis))
	if err != nil {
		log.Fatal(err)
	}
}
