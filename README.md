README for REST API in Golang  

Simply collect dependencies  
```bash
go get -d ./...
```
and build it with 

```bash
go build
```

After building run it with 

```bash
./REST-API
```

After running a server will start on localhost:8080  
You can run requests through localhost:8080/api  
POST for INSERT  
PUT for UPDATE  
DELETE for DELETE  
GET requests can be sent with respectable API_KEYS

